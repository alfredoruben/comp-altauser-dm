class CompAltauserDm extends Polymer.Element {

  static get is() {
    return 'comp-altauser-dm';
  }

  static get properties() {
    return {      
    _host: {
      type: String,
      value: 'http://clienteapi.us-west-1.elasticbeanstalk.com'
    },

    _path: {
      type: String,
      value: 'cliente'
    },

    _body: {
      type: Object,
      value: {}
    }};
  }
  addContact(contact) {
    console.info('cells-crud-dm::addContact.... begin');
    this.set('_path', 'cliente');
    this.set('_body',contact);
    this.$.cellsCrudPostDP.generateRequest();
    console.info('cells-crud-dm::addContact.... end');
  }
  _onPostResponse(evt) {
    console.info('cells-crud-dm::_onPostResponse.... begin');
    this.dispatchEvent(new CustomEvent("succes_event",{
      bubbles: true,
      composed: true,
      detail: {'success': evt}
    }));
    console.info('cells-crud-dm::_onPostResponse.... end');
  }
  _onPostError(err) {
    let detail = err.detail || null;

    if (detail !== null) {
      console.error('cells-crud-dm::_onPostError::err.detail: ', detail);
    }
  }
  
}

customElements.define(CompAltauserDm.is, CompAltauserDm);
